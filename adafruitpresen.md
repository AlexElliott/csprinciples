# Adafruit Circuit Presentation

Our project is twinkle twinkle little star. We were able to make the circuit play the notes of twinkle twinkle little star, and we got it to light up with each note making a sparkling.
We used a function for each line of the song, and then added them to a function that played the whole song. This made everything much easier to fix when there was a problem with the song


[starimage](starpic.jpeg)


We also were working on another project, which was a type of lock, where you had to hit the A and B buttons to show an animation. you had to hit the a button a certain amount of times to get half of the circuit to light up and than match that with the other button to show the animation. We were able to get the values to increase but we were not able to properly get the code to work. 




# Notes
* Edge computing collects data right at the source.
* This means there is higher quality data
* Increased Security
* Can use very specialized sensors
* Rasberry pi is an example of an edge computer
* The ESP32-Cam boards are very new
* There are more and more boards comming out
* The micro processors are commonly found in I-Phones
* Big Universities have large data repositories that you can use
* COnvelution is a filter that is used to add values of a pixel within an image to its neigboring pixels
* It makes feature maps
* Computers apply a filter to each pixel to search in more depth
* Quantization is taking an alalog signal and making it digital
* Pruning is similar to quantization but it cuts out the non needed items. It makes it less precise.
* The machine learning model will give you a percentage of how sure it is that it is the thing you are looking for.
* 70% correct is good in the professional industry
* TensorFlow Lite is an ESP32 Libray
* If you need to strenghten your model, add more data.
* The deveopment cycle is Idea--Data collection--Training--Deployment--Field Test
* Training is done on the cloud but in the future it will be all on one device 

# Comments
* I wonder what new boards will come out in the future
* It is interesting that the ML gives back a probability.

# Questions
* Why when we are developing faster boards are we sacraficing precision for efficiency?
* How will all of the training be compleated on the chip?
* What did it take to become a full stacks developer?


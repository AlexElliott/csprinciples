# Chapter 8 Notes


## Loops


### For 

* You are able to use a for loop to repeat the body of the loop a known number of times
* The body of the for loop are the lines that have been indented to be within the for loop
* The second line sets the variable equal to something
* You are able to have a for loop inside of a for loop and a while loop
* This is called a nested for loop
* The firmula to see how many times the nested for loop will run all you need to do is multipley the amount of times the outside loop will run with the inside times

### While

* A logical expression is either true or false
* A while loop will will repeat the body of a loop while a logical expression is true
* You are able to have an infinate loop if the logical expression is always true ( 1 == 1 )
* The body of the while loop are the lines that have been indented after the while statement
* While loops are used when you don't know haw many times you will need to run the loop
* The second line in a while loop tests if a variable is true or false depending on the parameter
* The logical expression will be checked before running the body everytime


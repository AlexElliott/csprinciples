- Abstraction: removing pieces of code that are no nessasary and to make code simpiler

- Analog data: Continuous information that varies over time.

- Bias: Systematic error or deviation from truth in data or model.

- Binary number system: Representation using only two digits, 0 and 1.

- Bit: Basic unit of digital information, can represent 0 or 1.

- Byte: Unit of measurement for digital information, typically composed of 8 bits.

- Classifying data: Sorting data into categories based on specific criteria.

- Cleaning data: Removing errors, inconsistencies, or irrelevant information from a data set.

- Digital data: Information represented in discrete form, usually as binary code.

- Filtering data: Selecting relevant information from a data set and discarding the rest.

- Information: Data that is processed, organized, and given meaning.

- Lossless data compression: Data compression technique where original data can be fully recovered without loss of information.

- Lossy data compression: Data compression technique where some information is lost to reduce the size of the data.

- Metadata: Data about data, including information about its content, structure, and organization.

- Overflow error: Error that occurs when a computation produces a result that is too large to be stored in the designated memory space.

- Patterns in data: Regularities or relationships in data that provide insights or support decision making.

- Round-off or rounding error: Error that occurs when a decimal number is rounded to a specific number of digits.

- Scalability: Ability of a system or solution to maintain its performance and effectiveness as the size or demand for it increases.

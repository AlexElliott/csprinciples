# Chapter 7 notes

## Repeating Steps

* Use a for loop to repeat code.
* Use range to create a list of numbers
* When a computer is repeating steps it is called an iteration or a loop

## Repeating With Numbers

* A for loop will use a variable and make the variable take on each of the values in a list of numbers one at a time 
* A list holds values in an order
* A line that has a for loop on it needs to end with :
* The statements inside the for loop must be indented be one more than the for loop
* If the statments are indented in the loop they will be run with the loop
* You always need at least one program in a body of a loop

## What is a list

* A list holds items in order
* A list is in between [] and is seperated by commas [1,2]
* A list has an order and each list item has a position in the list, like the first item in a list or the last item

### Using Better Variable Names

* Naming variables make it easier to program becasue you know what the variable will do just off the name (sum = 2 + 3)

## The Range Function

* the range function will loop over a sequence of numbers
* If the range function is called with a single positive integer it will generate all the integer values from 0 to one less than the number it was passed and assign them one at a time to the loop variable
* If there are two positive integers that are called with the range function it will generate all numbers starting with the lowest of the two and one less than the highest of the two
* for number in range(20, 31): (How to use the function)
* When using the range function you are also able to use 3 positive integers
* The first is the starting number the second is one above the ending number and the third is how much to change in between numbers
* You are able to count down with negative numbers

## There is a Pattern Here!

* There is a pattern that is very common when processing data
* It is called the Accumulator Pattern
* Here are the five steps in this pattern:
    1. Set the accumulator variable to its initial value. This is the value we want if there is no data to be processed.
    2. Get all the data to be processed.
    3. Step through all the data using a for loop so that the variable takes on each value in the data.
    4. Combine each piece of the data into the accumulator.
    5. Do something with the result.



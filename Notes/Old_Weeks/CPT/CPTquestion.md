# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
> The overall purpose of this program is to create a game for the user to play. The player moves a circle which attempts to avoid collision with robots.
>
2. Describes what functionality of the program is demonstrated in the
   video.
> The video shows the player moving and the robots moving toward the player and when the robots hit each other they turn into junk. When all robots are junk the game says "You Win!" and starts a new level where more robots are placed on the screen. When the user touches the robot it says "You've been caught!" and ends the game.
>
3. Describes the input and output of the program demonstrated in the
   video.
> The input is the user pressing keys to move the player and the output is the player and robots moving around on the graphics window.
>
## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
> ![list](img1.png)
> This code segment shows a list being created called "robots" on line 35. Then on lines 37-43 is it filling the list with robot objects. 
>
2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the programs purpose.
> ![list](img2.png)
> This code segment takes each robot in the list and moves them according to where the player is.
>

## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
> The name of the list is "robots".
>
2. Describes what the data contained in the list represent in your
   program
> The list represents all of the surviving robots.
> 
3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently if you did not use the list
> If the list was not in the code it would be difficult to have a lot of robots because they would have to be moved separately and that would take a lot of extra unneeded code.
>
## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.
1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedures name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
> ![list](img3.png)
> The name of the program is robot_crashed(the_bot). 
> It uses the_bot as its parameter and using that parameter it determinies if the robot has crashed.
> It has sequencing with the if statements, the selection also with the if statements, and it has iteration because it gets repeated every time check_collisions() is run.
>
2. The second program code segment must show where your student-developed
   procedure is being called in your program.
> ![list](img4.png)
> It is being called in check_collisions(). This makes the robot that is in the parameter into a jbot if the function robot_crashed() returns a_bot.
>

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
> It checks if a robot has crashed into another robot and if it has it returns the robot and if it hasn't it returns False. This allows for robots to crash and turn into junk which allows the player to beat the game.
>
4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
> It takes the parameter the_bot, and then for every a_bot in robots, it will test to see if a_bot is the same as the_bot. If it is it returns false, if it is not it does not do anything. It then tests to see if a_bot's x is the same as the_bot's x and if a_bot's y is the same as the_bot's y. If they are then it returns a_bot. If both are not true it returns False. 
>

## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.
> ![list](img5.png)
> First call:
> The first call of collided() is in safely_place_player() checks to see if the player will collide with any robots before it is placed and the game starts.
> 
> Second call:
> The second call of collided() is in check_collisions() where it checks to see if a robot has hit the player or if it has hit another robot.
>
2. Describes what condition(s) is being tested by each call to the procedure
>
> Condition(s) tested by the first call:
> It checks where the player is and where the robots are. If they are in the same spot then it outputs true.
>
> Condition(s) tested by the second call:
> It checks where the robots are and if they have collided with each other.
>
3. Identifies the result of each call
>
> Result of the first call:
> If the player is on a robot it removes the player from the screen and replaces them until they are no longer on top of a robot. 
>
> Result of the second call:
> If a robot hits another robot it removes one of them and turns the other into junk.
>

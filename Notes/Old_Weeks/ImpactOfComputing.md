# Vocab

## asymmetric ciphers
* It is a cipher with two keys, one for encryption and another one for decryption
## authentication
* the action of verifying the identity of a user
## bias
* an unreasoned and unfair distortion of judgement in favor or against a person or thing.
## Certificate Authority (CA)
* an internal or third party entity that makes, signs, and revokes digital certificates that blind public keys to user identities.
## citizen science
*  the collection and analysis of data relating to the world by the general public.
## Creative Commons licensing
* This allows the creator to retain compy right whilst allowing others to copy and distribute and make juse of their non-commerically
## crowdsourcing
* obtain an input or information by enlisting the services of a large number of people usually using the internet. 
## cybersecurity
* the state of being protected against the criminal or unauthorized use of electronic data.
## data mining
* the preactice of analyzing large databases in order to generate new information.
## decryption
* the conversion of encrypted data into its original form
## digital divide
* the divide of the people who have computers and access to the internet and those who don't
## encryption
* the process of converting information or data into a code, especially to prevent unauthorized access
## intellectual property (IP)
* invention that was made by someone using their own ideas, such as a manuscript or a design, to which one has rights and for which one may apply for a patent
## keylogging
* the usse of a computer program to record every keystroke made by a computers user. This is useed to gain access to passwords.
## malware
* a software that is specufically designed to damage or take data from a computer system.
## multifactor authentication
* Using multiple keys to verify a user.
## open access
* the unresticted right or oppertunity to use or benefit from something.
## open source
* denotiong software for which the original source code is made freely avalible and may be redistributed and modified.
## free software
* code that is publicy accessable
## FOSS
* Free Open Source Software
## PII (personally identifiable information)
* any data that can be used to identify someone
## phishing
* sending messages disgised as reputable companies in order to induce individuals to reveal personal information.
## plagiarism
* taking someone else's work and passing them off as your own.
## public key encryption
* a method of encrypting or signing data with two different keys and making one of the keys, the public key, available for anyone to use
## rogue access point
* an access point installed on a network without the network owner's permission
## targeted marketing
* a group of customers with shared demographics who have been identified as the most likely buyers of a company's product or service
## virus
* A computer program that can copy itself and infect a computer without permission or knowledge of the user

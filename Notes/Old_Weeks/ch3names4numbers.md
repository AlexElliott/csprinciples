# Chapter 3: Names for Numbers Notes


## Variables

* A variable is a name that the computer comes up with to represent a value
* It is called a variable because it can vary
* the value of a variable can be anything that can be represented on the computer
* Setting a variables value is called assignment
* You can have variables copy eachother such as b = 12    and  d = b so d = 12
* You can change a variables value by having = something else below it
* Tracing is a program that keeps track of the variables in the program and how their values change as the statements are executed

### Rules for a Variables Name

* Variables must start with a letter
* it can have numbers they just can be the first letter
* it can't be a python keyword
* there can not be any spaces
* it should help you understand what that variable stands for
* Instead of a space you can have an underscore

## Expressions

* When assigning a variable the right hand side does can be an arithmetic expression
* The computer can only store a certain number of digits for a fractional amount that repeats
* If you have expressions the variable that has the arithmatic expression has to be above where it was used

### Symbols for expressions

* + and - are addition and subtraction
* * and / are multiplication and division
* % is used to find only the remainder called modulo
* // is floor division which means if it can't be divided evenly then it will apear as 0

### How Expressions are Evaluated

 1. Operator
 2. Negation
 3. Multiplication, Division, and Modulo
 4. Addition and subtraction

## Function

* print can take an input (a variable)  whose value will be displayed
* print can also print a string

# Chapter 1 Notes

## 1.1
* The internet seems very easy to use but behind it there is a huge amount of code that took years to create.
* You can use wires to talk to a few people but it become hard to do with hundreds of people
* An operator would plug in the wire of the person you are trying to talk to
* To make this easier telephone companies had a bunch of central offices connected to eachother 
* The longer the wire the more expensive it is
* Only one person could be using a long distance cable at a time, thats why telephone companies charged by the minute to make long distance calls

## 1.2 
* Computers send messages to check if another computer is available
* In the old days computers were connected by wires
* Messages were sent through these wires
* Messages would wait for the messages infront of them to send first
* People could lease wires from telephone companies for their computer

## 1.3
* In the 1970s and the 1980s they found out that if a computer was connected to another computer that was connected to another computer it could be sent across long distances
* This method took a long time because the message had to wait its turn across the wires

## 1.4
* Another way to send messages was to send them in small "packets"
* If a packet of a short message was behind packets of a large message it would not have to wait for the whole large message to send
* There were computers that were specially meant to send and recive packets 
* They are now called routers but they used to be known as IMPs
* With IMPs you could send data across a Local Area Network to other computers hooked up to the IMP and it would send data to all the computers at once

## 1.5 
* In the early store and forward networks you needed to know the name of the computers
* This name was called the ip
* When a packet was sent it needed the source and destination labled onto the packet
* The packets needed a sort of instruction on how to put them back together

## 1.6 
* Routers send packets along a path to their destination
* Packets that were sent first can arrive later than packets sent after the first ones
* If a link on a path was broken routers can find another way to send the packets 
* A router can get data from a single computer or hundreds
* The term internet comes from the idea of interworking

## 1.7 Glossary
- **address**: A number that is assigned to a computer so that messages can be routed to the computer
- **hop**: A single physical network connection. A packet on the Internet will typically make several “hops” to get from its source computer to its destination.
- **LAN**: Local Area Network. A network covering an area that is limited by the ability for an organization to run wires or the power of a radio transmitter
- **leased line**: An “always up” connection that an organization leased from a telephone company or other utility to send data across longer distances.
- **operator (telephone)**: A person who works for a telephone company and helps people make telephone calls.
- **packet**: A limited-size fragment of a large message. Large messages or files are split into many packets and sent across the Internet. The typical maximum packet size is between 1000 and 3000 characters.
- **router**: A specialized computer that is designed to receive incoming packets on many links and quickly forward the packets on the best outbound link to speed the packet to its destination.
- **store-and-forward network**: A network where data is sent from one computer to another with the message being stored for relatively long periods of time in an intermediate computer waiting for an outbound network connection to become available.
- **WAN**: Wide Area Network. A network that covers longer distances, up to sending data completely around the world. A WAN is generally constructed using communication links owned and managed by a number of different organizations.

## 1.8 Questions 
1. What did early telephone operators do?
    - b) Connected pairs of wires to allow people to talk
2. What is a leased line?
    - d) An “always on” telephone connection
3. How long might a message be stored in an intermediate com- puter for a store-and-forward network?
    - d) possibly as long as several hours  
4. What is a packet?
    - c) A portion of a larger message that is sent across a network
5. Which of these is most like a router?
    - a) A mail sorting facility
6. What was the name given to early network routers?
    - d) Interface Message Processors
7. In addition to breaking large messages into smaller seg- ments to be sent, what else was needed to properly route each message segment?
    - a) A source and destination address on each message segment 
8. Why is it virtually free to send messages around the world using the Internet?
    - c) Because so many people share all the resources

# Chapter 2 Notes
* There are four layers of the internet the Link, Internetwork, Transport, and Application layers

## 2.1 
* The link layer is responseble for connecting your computer to its local network
* There are two problems the link layers needs to solve
    - how to encode data to send and how  
    - how to cooperate with other computers
* Wireless connection is more common than wire connection
* CSMA/CD stands for Carrier Sense Mulriple Acces with Collision Detection
* CSMA/CD all it does is listens to see if another computer is sending data on a network
* After your computer sends data it waits a little before sending more data

## 2.2
* The router looks at the destination address it chooses the best route for the packet but it is not perfect
* A packet goes through multiple hops
* Routers exchange messages about traffic delay or network outages

## 2.3 
* Packets contain an offset which tells the destination computer how to reconstruct the message
* If there are packets missing the destination computer will send a request for a resend of the missing packets
* The destination computer will acknowledgment when it recives a packet to the sending computer
* Window size is The amount of data that the sending computer is allowed to send before waiting for an acknowledgement.
* If the window size is too small, the transmission is slow, but if it is too big, it can overload routers.

## 2.4
* Each aplication is broken into two halfs
    - The server on the destination computer
    - The client on the source computer
* Chrome is a web client
* Application protocols describe how the messages are sent in the two halves  

## 2.5
* All four layers run on your computer and the source computer
* Routers operate at the Interwork and the Link layers
* When writing Networked applications you only talk to the Transportation Layer

## 2.6 Glossary
- **client**: In a networked application, the client application is the one that requests services or initiates connections.
- **fiber optic**: A data transmission technology that encodes data using light and sends the light down a very long strand of thin glass or plastic. Fiber optic connections are fast and can cover very long distances.
- **offset**: The relative position of a packet within an overall message or stream of data.
- **server**: In a networked application, the server application is the one that responds to requests for services or waits for incoming connections. 
- **window size**: The amount of data that the sending computer is allowed to send before waiting for an acknowledgement.

## 2.7 Questions
1. Why do engineers use a “model” to organize their approach to solving a large and complex problem?
    - c) Because they can break a problem down into a set of smaller problems that can be solved independently
2. Which is the top layer of the network model used by TCP/IP networks?
    - a) Application
3. Which of the layers concerns itself with getting a packet of data across a single physical connection?
    - d) Link 
4. What does CSMA/CD stand for?
    - a) Carrier Sense Multiple Access with Collision Detection
5. What is the goal of the Internetwork layer?
    - b) Get a packet of data moved across multiple networks from its source to its destination
6. In addition to the data, source, and destination addresses, what else is needed to make sure that a message can be reassembled when it reaches its destination?
    - a) An offset of where the packet belongs relative to the beginning of the message
7. What is "window size"?
    - d) The maximum amount of data a computer can send before receiving an acknowledgement
8. In a typical networked client/server application, where does the client application run?
    - a) On your laptop, desktop, or mobile computer
9. What does URL stand for?
    - c) Uniform Resource Locator
# Chapter 3
* We call the link layer the lowest layer because it is closest to the physical network media

## 3.1
* When your laptop is conected to WIFI it is sending data with a small radio
* Every computer connected to the router hears all the packets sent to and from the router
* All WIFI radio's have their own serial number
* The serial number is called the media access control
* A mac adress is like a to and from adress on a postcard
* Your computer sends a message to the WIFI to see what the MAC adress gateway is for the WIFI

## 3.2
* The first technique to avoid collisions when sending data is called the Carrier Sense
* The Carrier Sense is to listen first for a transmission and if there is one wait for it to end
* If two compter's data collied the packets are corrupted and both packets are lost
* If the WIFI radio assumes a collsion has happened it stops transmitting

## 3.3
* If there are many transmitting stations and they need to operate with 100% effciency they use a token
* The computers wait to recive the token and they will not transmit data without the token
* The token approach is best used for when it might take too long to detect a collision or too costly to detect a collision

## 3.4 
* A benefit of layered architecture is that engineers can focus on one layer at a time.

## 3.5 Glossary
- **base station**: Another word for the first router that handles your
packets as they are forwarded to the Internet. 
- **broadcast**: Sending a packet in a way that all the stations connected to a local area network will receive the packet.
- **gateway**: A router that connects a local area network to a wider
area network such as the Internet. Computers that want to send
data outside the local network must send their packets to the
gateway for forwarding.
- **MAC Address**: An address that is assigned to a piece of network
hardware when the device is manufactured.
- **token**: A technique to allow many computers to share the same
physical media without collisions. Each computer must wait until
it has received the token before it can send data. 

## 3.6 Questions
1. When using a WiFi network to talk to the Internet, where
does your computer send its packets?
    - a) A gateway
2. How is the link/physical address for a network device assigned?
    - c) By the manufacturer of the link equipment
3. Which of these is a link address?
    - a) 0f:2a:b3:1f:b3:1a
4. How does your computer find the gateway on a WiFi network?
    - b) It broadcasts a request for the address of the gateway
5. When your computer wants to send data across WiFi, what
is the first thing it must do?
    - a) Listen to see if other computers are sending data
6. What does a WiFi-connected workstation do when it tries to
send data and senses a collision has happened?
    - d) Stop transmitting and wait a random amount of time before
restarting
7. When a station wants to send data across a “token”-style
network, what is the first thing it must do?
    - d) Wait until informed that it is your turn to transmit
# Chapter 4
* To send data from your computer it needs to travel across multiple "hops"
* Routers have many links. Fiber optic, wireless, or saltalite 

## 4.1 
* We assign evert computer an adress based on where the computer is connected to a network
* IP numbers has to stay inbetween 0-255
* The first part of an IP adress is the network number
* The second part is the host number

## 4.2 
* Collapsing IP adresses into a single network number makes sending packets easier
* When a new core router is connected to the internet it asks its router neighbor to find a route for packets
* The routers make a map for sending packets 
* We call the map the Routing Table

## 4.3
* If a router incounters a problem it discards all of its entries in its routing table
* A two-connected network has two independent routes to get to a place
* Routers ask for other routers networking table
* The route your packets take from the source changes over time 

## 4.4 
* Most computers have something called traceroute
* It takes a pretty good guess on the route between your computer and the destination
* Routers can make a loop and it causes multiple routers to crash
* All packets have a number that is its time to live
* Every time a packet travels on a router the time to live number goes down
* Traceroute sends packets with different Time to live values
* Routers send back a message to the source computer when they throw away a packet

## 4.5 
* A computer gets a different IP adress when it moves networks
* When your computer asks the gateway what adress to use the gateway gives it a temporary IP
* If your computer has a self assigned IP adress it can not connect to the internet

## 4.6 
* Adresses that start with 192.168 are non-routable adresses
* non-routable addresses can not be used on the global network
* This saves real routable adresses

## 4.7 
* If you wanted to connect a new organization to the internet you would need to contact the Internet Service Provider
* The five Regional Internet Registries allocate IP adresses for a major geographic area
* We are transfering to IPv6 over time

## 4.9 Glossary
- **core router**: A router that is forwarding traffic within the core of
the Internet.
- **DHCP**: Dynamic Host Configuration Protocol. DHCP is how a
portable computer gets an IP address when it is moved to a new
location.
- **edge router**: A router which provides a connection between a
local network and the Internet. Equivalent to “gateway”.
Host Identifier: The portion of an IP address that is used to
identify a computer within a local area network.
- **IP Address**: A globally assigned address that is assigned to a
computer so that it can communicate with other computers that
have IP addresses and are connected to the Internet. To simplify
routing in the core of the Internet IP addresses are broken into
Network Numbers and Host Identifiers. An example IP address
might be “212.78.1.25”.
- **NAT**: Network Address Translation. This technique allows a single
global IP address to be shared by many computers on a single
local area network.
- **Network Number**: The portion of an IP address that is used to
identify which local network the computer is connected to.
- packet vortex: An error situation where a packet gets into an
infinite loop because of errors in routing tables.
- **RIR**: Regional Internet Registry. The five RIRs roughly correspond
to the continents of the world and allocate IP address for the major geographical areas of the world.
routing tables: Information maintained by each router that
keeps track of which outbound link should be used for each
network number.
- **Time To Live (TTL)**: A number that is stored in every packet
that is reduced by one as the packet passes through each router.
When the TTL reaches zero, the packet is discarded.
traceroute: A command that is available on many Linux/UNIX
systems that attempts to map the path taken by a packet as it
moves from its source to its destination. May be called “tracert”
on Windows systems.
- **two-connected network**: A situation where there is at least two
possible paths between any pair of nodes in a network. A twoconnected network can lose any single link without losing overall
connectivity.

## 4.10 Questions
1. What is the goal of the Internetworking layer?
    - a) Move packets across multiple hops from a source to destination computer
2. How many different physical links does a typical packet cross
from its source to its destination on the Internet?
    - c) 15
3. Which of these is an IP address?
    - b) 192.168.3.14
4. Why is it necessary to move from IPv4 to IPv6?
    - c) Because we are running out of IPv4 addresses
5. What is a network number?
    - a) A group of IP addresses with the same prefix
6. How many computers can have addresses within network
number “218.78”?
    - c) 65000
7. How do routers determine the path taken by a packet across
the Internet?
    - b) Each router looks at a packet and forwards it based on its
best guess as to the correct outbound link
8. What is a routing table?
    - d) A list of network numbers mapped to outbound links from
the router
9. How does a newly connected router fill its routing tables?
    - d) By asking neighboring routers how they route packets
10. What does a router do when a physical link goes down?
    - a) Throws away all of the routing table entries for that link
11. Why is it good to have at least a “two-connected” network?
    - d) Because it continues to function even when a single link
goes down
12. Do all packets from a message take the same route across
the Internet?
    - b) No
13. How do routers discover new routes and improve their routing tables?
    - b) They periodically ask neighboring routers for their network
tables
14. What is the purpose of the “Time to Live” field in a packet?
    - a) To make sure that packets do not end up in an “infinite loop”
15. How does the “traceroute” command work?
    - a) It sends a series of packets with low TTL values so it can get
a picture of where the packets get dropped
16. About how long does it take for a packet to cross the Pacific
Ocean via an undersea fiber optic cable?
    - b) 0.025 Seconds
17. On a WiFi network, how does a computer get an Internetworking (IP) address?
    - a) Using the DHCP protocol
18. What is Network Address Translation (NAT)?
    - d) It reuses special network numbers like “192.168” across multiple network gateways at multiple locations
19. How are IP addresses and network numbers managed globally?
    - a) There are five top-level registries that manage network numbers in five geographic areas
20. How much larger are IPv6 addresses than IPv4 addresses?
    - c) IPv6 addresses are twice as large as IPv4 addresses
21. What does it mean when your computer receives an IP address that starts with “169..”?
    - b) The gateway is mapping your local address to a global address using NAT
22. If you were starting an Internet Service Provider in Poland,
which Regional Internet Registry (RIR) would assign you a
block of IP addresses.
    - c) RIPE NCC

## Mr. Elkners Questions
1. 10011110.00111011.11100001.01110011
2. He gets this number from the front of the IP adress and it is a Network Number. It is used for computers connected to that network and it saves IP adresses as there are only so many that we can use. Networks give out a temporary IP to any new computer on the network, these IPs usually having a network number infront of them. 

# Chapter 5
* The domain name system allows you to acess websites through a domain not a IP adress
* When a computer uses a website it looks up the IP adress that coresponds with the domain
* A server can be moved without affecting the end user

## 5.1
* Top of the domain name hierarchy is an organization called **International Corporation for Assigned Network Names and Numbers** (ICANN)
* ICANN assigns top-level domains like .com, .edu, .org, .club, and .help.
* They also assign country code domains
* Country code names are called Country-Code Top-Level Domain Names
* Once a domain name is assigned to an organization they can make subdomains within that domain

## 5.2
* The left part of the IP adress is broad and the right is specific
* You read a domain name left to right as it gets more specific the more right you go

## 5.3
* The Domain name is not part of the whole system but it makes usuing the internet easier
* It makes moving to one internet connection to another much easier

## 5.4 Glossary
- **DNS**: Domain Name System. A system of protocols and servers
that allow networked applications to look up domain names and
retrieve the corresponding IP address for the domain name.
- **domain name**: A name that is assigned within a top-level domain. For example, khanacademy.org is a domain that is assigned
within the “.org” top-level domain.
- **ICANN**: International Corporation for Assigned Network Names
and Numbers. Assigns and manages the top-level domains for
the Internet.
- **registrar**: A company that can register, sell, and host domain
names.
- **subdomain**: A name that is created “below” a domain
name. For example, “umich.edu” is a domain name and
both “www.umich.edu” and “mail.umich.edu” are subdomains
within “umich.edu”.
- **TLD**: Top Level Domain. The rightmost portion of the domain
name. Example TLDs include “.com”, “.org”, and “.ru”. Recently,
new top-level domains like “.club” and “.help” were added.

## 5.5 Questions
1. What does the Domain Name System accomplish?
    - a) It allows network-connected computers to use a textual
name for a computer and look up its IP address
2. What organization assigns top-level domains like “.com”,
“.org”, and “.club”?
    - c) ICANN - International Corporation for Assigned Network
Names and Numbers
3. Which of these is a domain address?
    - c) www.khanacademy.org
4. Which of these is not something a domain owner can do with
their domain?
    - c) Create new top-level domains

# Chapter 6
* The transport layer does not attempt to guarantee the delivery of a packet

## 6.1
* There is a Link header - IP header - TCP header - Data Packet
* A lik header is removed when it is recived on one link and a new header is added when it is sent to a new link
* The TCP indicates where the data in each packet belongs 

## 6.2
* When the destination computer gets packets it looks at the offset to reassemble the file
* If the transport layer recives a packet that was sent later it places the packet into a buffer
* The pause to wait for an acknowledgment is called a windo size
* By adjusting the window size it becomes easier to send data quickly
* If a packet is lost it will never go to the destination computer
* the destination computer sends out a packet to the sending computer indicating where it last saw data
* the sending computer will send more data at the destination computer after reciving the packet

## 6.3
* The sending computer must hold onto all data untill the destination computer has all of it
* reciving a packet out of order is not a problem 

## 6.4
* the purpose of the transport layer is to provide a reliable connection between two computers
* the application that initiates the connection is called the client
* the application that responds to the connection is called the server

## 6.5
* A remote computer must connect to the correct webserver to access it
* A client application must know what server to connect to
* We use ports to allow a client application to choose which server to connect to
* 8080 means your browser is going to use web protocals to interact with the server and use port 8080 

## 6.6 
* The transport layer is meant to make up for the two lower layers when they lose or reroute packets

## 6.7 Glossary
- **acknowledgement**: When the receiving computer sends a notification back to the source computer indicating that data has
been received.
- **buffering**: Temporarily holding on to data that has been sent or
received until the computer is sure the data is no longer needed.
- **listen**: When a server application is started and ready to accept
incoming connections from client applications.
- **port**: A way to allow many different server applications to be
waiting for incoming connections on a single computer. Each
application listens on a different port. Client applications make
connections to well-known port numbers to make sure they are
talking to the correct server application.

## 6.8 Questions
1. What is the primary problem the Transport (TCP) layer is supposed to solve?
    - c) Deal with lost and out-of-order packets
2. What is in the TCP header?
    - c) Port number and offset
3. Why is “window size” important for the proper functioning of
the network?
    - b) It prevents a fast computer from sending too much data on
a slow connection
4. What happens when a sending computer receives an acknowledgement from the receiving computer?
    - b) The sending computer sends more data up to the window
size
5. Which of these detects and takes action when packets are
lost?
    - d) Receiving computer
6. Which of these retains data packets so they can be retransmitted if a packets lost?
    - a) Sending computer
7. Which of these is most similar to a TCP port?
    - c) Apartment number
8. Which half of the client/server application must start first?
    - a) Client
9. What is the port number for the Domain Name System?
    - c) 53
10. What is the port number for the IMAP mail retrieval protocol?
    - d) 143

# Chapter 7
* the application layer is where web browsers work

## 7.1
* To browse a web adress you must have a web application running on your computer 
* There are two parts for a networked application to work Client/Server 
* your computer looks up the IP adress for the server 

## 7.2
* Each pair of network applications has rules on how to govern the conversation
* the set of rule that govern how we communicate is called the protocol
* Some protocols are intricate and complex while others are very simple
* https is a type of protocol
* https is relativly simple thats why it is popular 

## 7.3
* The internet was created in 1985
* Telnet was made before the first TCP/IP network was in production
* port 80 is where you find a webserver
* if there is no webserver on port 80 the connection fails
* A status code of 200 means things went well
* A status code of 404 means that the document was not found
* A status code of 301 means the document has been moved
* The status codes are sorted into ranges 2xx is good, 3xx are for redirecting, and 4xx is the client did something wrong, 5xx is the server did something wrong

## 7.4
* HTPP is one of the main client/server applocation protocols used onto the internet
* your mail gets sent to a server while your computer is off
* IMAP is a more complicated protocol that can not be faked
* the messages sent by the client and the server are not discriptive as they are not meant to be seen by the end user 

## 7.5
* the transport layer waits for acknowledgement
* stopping and starting the sending for the application is called flow control
* The applications are not responable for flow control 

## 7.6
* In python it is easy to connect to a web server
* sending data is not a hard task to do
* Because the layers all work seemlessly it makes sending and reciving data easy

## 7.7 
* The lower layers make the application layers job easier and allows it to focus on applications and nothing else
* Because of this we have a wide range of network applications

## 7.8 Glossary
- **HTML**: HyperText Markup Language. A textual format that
marks up text using tags surrounded by less-than and
greater-than characters. Example HTML looks like: <p>This
is <strong>nice</strong></p>.
- **HTTP**: HyperText Transport Protocol. An Application layer protocol that allows web browsers to retrieve web documents from web
servers.
- **IMAP**: Internet Message Access Protocol. A protocol that allows
mail clients to log into and retrieve mail from IMAP-enabled mail
servers.
- **flow control**: When a sending computer slows down to make
sure that it does not overwhelm either the network or the destination computer. Flow control also causes the sending computer
to increase the speed at which data is sent when it is sure that
the network and destination computer can handle the faster data
rates.
- **socket**: A software library available in many programming languages that makes creating a network connection and exchanging data nearly as easy as opening and reading a file on your
computer.
- **status code**: One aspect of the HTTP protocol that indicates
the overall success or failure of a request for a document. The
most well-known HTTP status code is “404”, which is how an HTTP
server tells an HTTP client (i.e., a browser) that it the requested
document could not be found.
- **telnet**: A simple client application that makes TCP connections
to various address/port combinations and allows typed data to be
sent across the connection. In the early days of the Internet, telnet was used to remotely log in to a computer across the network.
- **web browser**: A client application that you run on your computer
to retrieve and display web pages.
- **web server** : An application that deliver (serves up) Web pages

## 7.9 Questions
1. Which layer is right below the Application layer?
    - a) Transport
2. What kind of document is used to describe widely used Application layer protocols?
    - b) RFC
3. Which of these is an idea that was invented in the Application layer?
    - d) http://www.dr-chuck.com/
4. Which of the following is not something that the Application
layer worries about?
    - c) How the window size changes as data is sent across a socket
5. Which of these is an Application layer protocol?
    - a) HTTP
6. What port would typically be used to talk to a web server?
    - b) 80
7. What is the command that a web browser sends to a web
server to retrieve an web document?
    - d) GET
8. What is the purpose of the “Content-type:” header when you
retrieve a document over the web protocol?
    - a) Tells the browser how to display the retrieved document
9. What common UNIX command can be used to send simple
commands to a web server?
    - d) telnet
10. What does an HTTP status code of “404” mean?
    - d) Document not found
11. What characters are used to mark up HTML documents?
    - a) Less-than and greater-than signs < >
12. What is a common application protocol for retrieving mail?
    - d) IMAP
13. What application protocol does RFC15 describe?
    - a) telnet
14. What happens to a server application that is sending a large
file when the TCP layer has sent enough data to fill the window size and has not yet received an acknowledgement?
    - c) The application is paused until the remote computer
acknowledges that it has received some of the data
15. What is a “socket” on the Internet?
    - d) A two-way data connection between a pair of client and
server applications
16. What must an application know to make a socket connection
in software?
    - a) The address of the server and the port number on the server

# Chapter 8
* In the early days there was no need to protect data from prying eyes
* When we started using wireless technology we needed network security
* There are two ways to protect network activity 
    - One is to protect the physical hardware
    _ Another is to encrypt the data

## 8.1
* In the roman era they would encrypt messages with the Ceaser Cypher
* All encryption has a key to solving it

## 8.2
* They used to call one another to tell eachother the keys to unencrypt the data
* in the 1970s they came up with a assymetric key which meant one key encrypted the data and different on decrypted the data

## 8.3
* Network engineers did not want to mess up the existing internet protocols so they added a partial layer between the Transport and the Application layers.
* The application layer can request the transport layer to send data encrypted or not encrypted

## 8.4 
* Web browsers use http or https to indicate that they want the data encrypted
* your browser shows a lock on the left by the domain name to show that it is encrypted
* https used to be expensive but now is becoming cheaper and being used for everything

## 8.5
* A rogie computer can send you a public key and can take your data when you send back your encypted data as they have the key to decrypt it, but this key only comes from websites posing to be other websites
* To confirm you are on a trusted site they will send you a public key signed by the Certificate Authority
* Your browser on download knows a bunch of trustable Certificate Authorities

## 8.6
* Secure connections on the internet are called **Secure Sockets Layer** or the **Transport Layer Security**
* By having the security be in the transport layer it allows the other layers to stay the same

## 8.7 Glossary
- **asymmetric key**: An approach to encryption where one (public)
key is used to encrypt data prior to transmission and a different
(private) key is used to decrypt data once it is received.
- **certificate authority**: An organization that digitally signs public
keys after verifying that the name listed in the public key is actually the person or organization in possession of the public key.
- **ciphertext**: A scrambled version of a message that cannot be
read without knowing the decryption key and technique.
- **decrypt**: The act of transforming a ciphertext message to a plain
text message using a secret or key.
- **encrypt**: The act of transforming a plain text message to a ciphertext message using a secret or key.
- **plain text**: A readable message that is about to be encrypted
before being sent.
- **private key**: The portion of a key pair that is used to decrypt
transmissions.
- **public key**: The portion of a key pair that is used to encrypt
transmissions.
- **shared secret**: An approach to encryption that uses the same
key for encryption and decryption.
- **SSL**: Secure Sockets Layer. An approach that allows an application to request that a Transport layer connection is to be encrypted as it crosses the network. Similar to Transport Layer Security (TLS).
- **TLS**: Transport Layer Security. An approach that allows an application to request that a Transport layer connection is to be encrypted as it crosses the network. Similar to Secure Sockets Layer
(SSL).

## 8.8 Questions
1. How do we indicate that we want a secure connection when
using a web browser?
    - a) Use https:// in the URL
2. Why is a shared-secret approach not suitable for use on the
Internet?
    - b) It is difficult to distribute the secrets
3. What is the underlying mathematical concept that makes
public/private key encryption secure?
    - d) Prime numbers
4. Which of the keys can be sent across the Internet in plain
text without compromising security?
    - a) Encryption key
5. Where does the Secure Sockets Layer (SSL) fit in the fourlayer Internet architecture?
    - d) Between the Transport and Application layers
6. If you were properly using https in a browser over WiFi in a
cafe, which of the following is the greatest risk to your losing
credit card information when making an online purchase?
    - d) You have a virus on your computer that is capturing
keystrokes
7. With the Secure Sockets Layer, where are packets encrypted
and decrypted?
    - c) They are encrypted in your computer and decrypted in the
server
8. What changes to the IP layer were needed to make secure
socket layer (SSL) work?
    - a) No changes were needed
9. If a rogue element was able to monitor all packets going
through an undersea cable and you were using public/private key encryption properly, which of the following
would be the most difficult for them to obtain?
    - d) Which documents you retrieved from the servers
10. What is the purpose of a Certificate Authority in public/private key encryption?
    - c) To assure us that a public key comes from the organization
it claims to be from
11. The ARPANET network was in operation starting in the 1960s.
Secure Sockets Layer (SSL) was not invented util the 1980s.
How did the ARPANET insure the security of the data on its
network?
    - c) By making sure no one could access the physical links
12. Which of these answers is “Security is fun” encrypted with a
Caesar Cipher shift of 1.
    - c) Tfdvsjuz jt gvo
13. What Caesar Cipher shift was used to encrypt “V yvxr frphevgl”?
    - c) 13



# Study Plan




## How is the APCSP exam scored?


### There are Two Sections One for Multiple Choice and one For the Create Performance Task

### Multiplce choice
* The Multiple Choice is 70 questions and is worth 70% on the total score for the exam
* In the multiple choice questions there are 
    * 57 single select multiple choice 
    * 5 single select with reading passage about a computing innovation
    * 8 multiiple select multiple choice: select 2 answers


### What is the Create Performance Task and how is it scored?

* The Create Performace Task is a task where the student has to write a computer program accompanied by a video and a writen response
* The task will focus specifically on the creation of a computer program
* The Create Performace Task is worth 30% of the APCSP exam
* The Create Performance Task is scored on six points
    * Descrbing the overall purpose and the function of your program
    * Testing your program to make sure it works for its intended functionality
    * Proper Use of algorithms
    * Showing how your program uses data abstraction
    * Demonstraighting how your program uses procedural abstraction
    * Explaining how complexity is managed in the program

## How many of the 70 multiple choice questions do I need to get correct to earn a 5, 4, or 3 on the exam?
If you get all 70 questions correct but get a 0 on the Create Performance task, you will only get a 3. If you get a 
3/6 on the Create Performance task you will earn a 4 with 70 questions correct on the multiple choice exam. To generally
get a 4 you need to score a 56/70 on the multiple choice test or above and a 5/6 on the Create Performance task.

## On which of the 5 Big Ideas did I score best?

I scored best on Big Idea 5.

## On which do I need improvment?

I need to improve Big Ideas 1-4.

## What online resources are avalible to help me prepare for the exam in each of the big idea areas?

[Score Calculator](https://www.albert.io/blog/ap-computer-science-a-score-calculator/)

[Helpful Study Guide](http://support.ebsco.com/LEX/AP-Computer-Science-A_Study-Guide.pdf)

[Very Helpful Github Study Guide](https://ap-computer-science-guide.github.io/)

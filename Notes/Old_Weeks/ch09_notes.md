# Chapter 9 Notes

## Somethings To Remember

* Strings is a collection of letters, digits, and other characters.
* When you have a string and a varable and you want to combine them in a print function you can use +
``` 
name = Alex 
print("Hey" + name)
```
This will print:
    Hey Alex
## The Accumulator pattern

1. Set the accumulator variavle to its initial value. This is the value we want if there is no data to be processed
2. Get all the data to be processed
3. Step through all the data using a **for** loop so that the variable takes on each value in the data.
4. Combine each piece of the data into the accumulator
5. Do something with the result

### Here is an example of this pattern at work
```
# Step 1: Initialize accumulator
new_string = ""

# Step 2: Get data
phrase = "Rubber baby buggy bumpers."

# Step 3: Loop through the data
for letter in phrase:
    # Step 4: Accumulate
    new_string = new_string + letter

# Step 5: Process result
print(new_string)
```
This will print:
    Rubber baby buggy bumpers.
## Reverse Text

When Using the Accumulator pattern you are able to reverse the text very easily. All you need to do is switch the new string + letter to letter + new string
```
# Step 1: Initialize accumulators
new_string_a = ""
new_string_b = ""

# Step 2: Get data
phrase = "Happy Birthday!"

# Step 3: Loop through the data
for letter in phrase:
    # Step 4: Accumulate
    new_string_a = letter + new_string_a
    new_string_b = new_string_b + letter

# Step 5: Process result
print("Here's the result of using letter + new_string_a:")
print(new_string_a)
print("Here's the result of using new_string_b + letter:")
print(new_string_b)

```
This will print:
    Here's the result of using letter + new string a:
    !yadhtriB yppaH
    Here's the result of using new string b + letter:
    Happy Birthday!

The reason it is backwards is because each letter gets added at the beginning instead of the end which creates a reversal.


## Mirroring Text

```
# Step 1: Initialize accumulator
new_string = ""

# Step 2: Get data
phrase = "This is a test"

# Step 3: Loop through the data
for letter in phrase:
    # Step 4: Accumulate
    new_string = letter + new_string + letter

# Step 5: Process result
print(new_string)
```
This will print:
    tset a si sihTThis is a test


This gets printed becasue you are adding a letter to both the beggining and end of the string
### You are able to start the string with something in it it does not have to be empty

## Modifying Text
* We are able to loop through the string and use the **find** and slice substring

```
a_str = "He wanted a peice of candy"
a_str = a_str + " so he gave her a peice."
pos = a_str.find("peice")
while pos >= 0:
    a_str = a_str[0:pos] + "piece" + a_str[pos+len("peice"):len(a_str)]
    pos = a_str.find("peice")
print(a_str)
```
This will print:
    He wanted a piece of candy so he gave her a piece.

## Summary

* ***Accumulator Pattern*** - The accumulator pattern is a set of steps that processes a list of values. One example of an accumulator pattern is
the code to reverse the characters in a string.
* ***Palindrome*** - A palindrome has the same letters if you read it from left to right as it does if you read it from right to left. An example is "A but tuba".
* ***String*** - A string is a collection of letters, numbers, and other characters like spaces inside of a pair of single or double quotes.
* ***def*** - The ***def*** keyword is used to define a procedure or function in Python. The line must also end with a ***:*** and the body of the procedure or function must be indented 4 spaces.
* ***for*** - A ***for*** loop is a programming statement that tells the computer to repeat a statement or a set of statements. It is one type of loop.
* ***print*** - The ***print*** statement in Python will print the value of the items passed to it.
* ***range*** - The ***range*** function in Python returns a list of consecutive values. If the range function is passed one value it returns a list with the numbers from 0 up to and not including the passed number. For example, ***range(5)*** returns a list of ***[0, 1, 2, 3, 4]***. If the range function is passed two numbers separated by a comma it returns a list including the first number and then up to but not including the second number. For example, ***range(1, 4)*** returns the list ***[1, 2, 3]***. If it is passed three values ***range(start, stop, step)*** it returns all the numbers from start to one less than end changing by step. For example, ***range(0, 10, 2)*** returns ***[0, 2, 4, 6, 8]***.
* ***while*** - A while loop is a programming statement that tells the computer to repeat a statement or a set of statements. It repeats the body of the loop while a logical expression is true.

# Bitwise Operators


##  This table is a table full of the Bitwise Operators
|Operator|Example|Meaning|Expalanation|Example|
| :---: | :---: | :---: | :---: | :---:|
|&|a & b|Bitwise AND|operator returns a 1 in each bit position for which the corresponding bits of both operands are 1s|1010 & 0010 = 0010|
|\||a\|b|Bitwise OR|operator returns a 1 in each bit position for which the corresponding bits of either or both operands are 1s| 1010 \| 0001 = 1011|
|^|a^b|Bitwise XOR (exclusive OR)|operator returns a 1 in each bit position for which the corresponding bits of either but not both operands are 1s|1010 ^ 0011 = 1001|
|~|a~b|Bitwise NOT|operator inverts the bits of its operand. Like other bitwise operators, it converts the operand to a 32-bit signed integer| 1010 ~ 1110 = 0101|
|<<|a << n|Bitwise left shift|You shift the first bit to the left by the value of the second bit.| 1010 << 0001 = 0100|
|>>|a >> n|Bitwise right shift|You shift the first bit to the right by the value of the second bit.| 1010 >> 0001 = 0101|

## All Bitwise have a corresponding compound operator
|Operator|Example|Equivilent to|
| :---: | :---: | :---: |
|&=|a &= b|a = a & b|
|\=||a\|=b|a = a \| b|
|^=|a^=b|a = a ^ b|
|~=|a~=b|a = a ~ b|
|<<=|a <<= n|a = a << n|
|>>=|a >>= n|a = a >> n|

# Logic Tables (Truth Tables)

# &
|A|B|Output|
| :---: | :---: | :---: |
| 1 | 1 |1 |
| 1 | 0 |0 |
| 0 | 1 |0 |
| 0 | 0 |1 |

# |
|A|B|Output|
| :---: | :---: | :---: |
| 1 | 1 |1 |
| 1 | 0 |1 |
| 0 | 1 |1 |
| 0 | 0 |0 |

# ^
|A|B|Output|
| :---: | :---: | :---: |
| 1 | 1 |0 |
| 1 | 0 |1 |
| 0 | 1 |1 |
| 0 | 0 |0 |

# ~
|A|B|Output|
| :---: | :---: | :---: |
|1010|0101|1111 |
|1101|0110|1011 |
|0000|1111|1111 |
|1001|0100|1101 |

# <<
|A|B|Output|
| :---: | :---: | :---: |
|1010|0001|0100 |
|1101|0010|0100 |
|0001|0011|0100 |
|1001|0100|1000 |

# >>
|A|B|Output|
| :---: | :---: | :---: |
|1010|0001|0101 |
|1101|0010|0011 |
|1000|0011|0010 |
|1001|0100|0001 |





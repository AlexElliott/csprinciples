# Software Documentation Should Include 
- [Requirements](#requirements)
  - What is needed to run the program.
  - Such as gpu, keyboard, internet, etc.
- [Constraints](#constraints)
  - What the program can do
  - What will make it have bugs, e.g., having to enter a place holder for entry boxes you'd like to leave blank
  - What are the software limitations e.g. not enough power to run something
- [Purpose](#purpose)
  - What the program does
  - Why people should use the program

## Types of Software Documentation
1. User Focused Documentation
  - This is easier to read and used for testers and and-users when they use your software. 
2. Developer Focused Documentation
- This is more technical and more indepth with the code. It is used for developers, proggrammers, project managers, and shareholders. Without experience in the industry these documents will be hard to understand.

<div id='requirements'/>

## Program requirements

### How to include 
  Have a paragraph explaining how the code works and what is used for it and when it is used in the code. 
### What to say
Spec lists are a common method for consicely describing hardware requirements

    Requirements:
     • 4GB GPU
     • 106 GB/s bandwidth
     • 10 GB disk space

<div id='constraints'/>

## Program Constraints

### How to include
In the documentation you should have a list of contraints.
### What to say
  You should say what the program can't do. Like it might only work on one operating system, or it cannot do something that people will think your software will be able to do.

<div id='purpose'/>

## Program Purpose
It should explain what the program is for and how you are going to use it.

### How to include
It should be one of the first things you see on documentation, as it says what it is about.

### What to say
Write a paragraph describing what the program is best used for, also include a cons.

## General Structure

### What order should the categories be listed?
1. Program Purpose
2. Program Requirements
3. Program Constraints

### Style and Best Practices
Have information categorized by where in the program it's relevent to, who it's relevent to, and what type of information it is. Use proper grammar. Include examples and e

### Some Free Templates
There are many sites that you can use to get free Documentation templates. These are just a few of them:
* [Slite's free template](https://slite.com/templates/software-documentation)
* [CoderDocs free template](https://ultraviolet.meaticus22.repl.co/service/hvtrs8%2F-tjeoeq.1rfwcvgmgdka%2Ccmm-bmovsvrcp%2Ftgmrlctgs-svaptwp-cmdgrfoas%2Ffpeg-%60omtqtpar-6-foauoeltctkol-veopnave%2Ffmr%2Fsmfvwcrg-rrmjgcvs-)
* [Scribblers free template](https://ultraviolet.meaticus22.repl.co/service/hvtrs8%2F-t%7Bmraluq.lev%2Faofrmpq%2F0038-03%2F32-fpegbke%2Fsarkb%60lgr%2Fwgbqive%2Ftgmrlctg-jtol%2Fsievcj%2F)
![Template Example](qwe.png)

# Crowdsourcing and Citizen Science

## Definitions

### Crowd Sourcing
* Obtain information or an opinion on a topic by getting the services of a large number of people, they can be paid or unpaid, and it is usually through the internet.
### Citizen Science
* Recruiting members of the public to help conduct experiments and collect data, usually with the help of a professional



## Crowd Sourcing

### For what purpose?
Crowdsourcing is most useful when a organization is doing a task that requires far more time or manpower than they have on hand. In this case crowsourcing can be used to offhand the work to the general public.

### Can I do crowd sourcing?
Absolutely! Infact, you probably already have. If you've ever done a survey you've participated in crowdsourcing.
![Would you eat it?](potatochips.png)

### What are the types of crowdsourcing?
* Crowd Creation : This is when an organization has people create ideas for them. McDonalds has customers create custom burgers for them and Lego has people submit custom lego set ideas on their website.
* Crowd Voting : This next one is when an organization has people vote on their ideas. Lets say a company can only afford to bring one products to market, but have created three ideas. They might have consumers vote on the product they would prefer.
* Crowd Funding : The final form of crowdsourcing is *Crowd Funding*, it requires no additional explenation. It is simply when a company sources capital from members of the general public.
![crowdsourcing](purplecrowd.png)


### Examples
* Nasa's SETI crowsources the background computing power of thousands of willing civilians computers to analyze radio waves from space.
* Waze is a phone app that warns people of traffic and such by having the apps users submit their location to the app whenever they're in a traffic jam and let the app track their speeds. They do this to gain information on traffic so that their app works.

## Citizen Science

### What is Citizen Science Exactly?
Citizen Science is when citizens volunteer to help conduct science experiments.

### Who can do Citizen Science
Anyone can do it, and it doesn't matter how old you are.
![sadkid](sadkid.png)

### How does one start to do Citizen Science?
If you look up the National Parks Service Website you are able to find plenty of projects near you to help with, and they give you all the tools you need.

### Why do we do Citizen science?
Citizen science is important for democratizing science and promoting the goal of universal access to scientific data and information.

### Does anyone actual use the data?
Yes! Citizen science data is being used extensively for studies of biodiversity and pollution and a lot of citizen science is being used for monitoring the sustainable development goals.

### Examples of Citizen Science
* Stardust@home
  * In 2006 a capsule from NASA's Stardust spacecraft returned to Earth after collecting small particles of dust from stars. Interstellerddust particals are about a micron in size and it would take forever for scientists to find all of them, so instead they uploaded it to the internet and let volunteers help search for them.
* Globe at Night
  * Globe at Night  measures the impact of light pollution. Citizen scientists calculate the brightness of the sky where they live allowing experts to understand the effects of ploution at night.
![globenight.png](globenight.png)

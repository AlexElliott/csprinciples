from gasp import *
from random import randint
junk = []

class Player:
    pass

class Robots:
    pass

def place_player():
    global player, key
    player = Player()
    player.x = randint(5,63)
    player.y = randint(5,47)
    player.c = Circle((player.x, player.y), 5)

def move_player():
    global player, key
    key = update_when('key_pressed')
    if key == 'q':
        player.y = player.y + 1
        player.x = player.x - 1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 'e':
        player.y = player.y + 1
        player.x = player.x + 1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 'z':
        player.y = player.y - 1
        player.x = player.x - 1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 'c':
        player.y = player.y - 1
        player.x = player.x + 1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 'w':
        player.y = player.y + 1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 'a':
        player.x = player.x - 1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 'd':
        player.x = player.x + 1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 's':
        player.y = player.y - 1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 'r':
        remove_from_screen(player.c)
        safely_place_player()

def place_robots():
    global robot, numbots, robots
    robots = []
    while len(robots) < numbots:
        robot = Robots()
        robot.x = randint(5,63)
        robot.y = randint(5,47)
        if not collided(robot, robots):
            robot.c = Box((10 * robot.x + 5, 10 * robot.y + 5), 10, 10, filled=True)
            robots.append(robot)

def move_robots():
    global robots, player
    for robot in robots:
        if robot.x < player.x and robot.y < player.y:
            robot.x = robot.x + 1
            robot.y = robot.y + 1
        elif robot.x > player.x and robot.y > player.y:
            robot.x = robot.x - 1
            robot.y = robot.y - 1
        elif robot.y < player.y and robot.x > player.x:
            robot.y = robot.y + 1
            robot.x = robot.x - 1
        elif robot.y > player.y and robot.x < player.x:
            robot.y = robot.y - 1
            robot.x = robot.x + 1
        elif robot.x < player.x:
            robot.x = robot.x + 1
        elif robot.x > player.x:
            robot.x = robot.x - 1
        elif robot.y < player.y:
            robot.y = robot.y + 1
        elif robot.y > player.y:
            robot.y = robot.y - 1
        move_to(robot.c, (10 * robot.x + 5, 10 * robot.y + 5))

def check_collisions():
    global numbots, lvl, jbot, player, junk, robots, finished
    surviving_robots = []
    for robot in robots:
        if collided(robot, junk):
            continue
        if collided(player, robots+junk) == True:
            Text("You've been caught!", (200,240), size=36)
            sleep(3)
            finished = True
            return
        jbot = robot_crashed(robot)
        if jbot == False:
            surviving_robots.append(robot)
        else:
            remove_from_screen(robot.c)
            jbot.shape = Box((10*jbot.x, 10*jbot.y), 10, 10)
            junk.append(jbot)
    robots = []
    for live in surviving_robots:
        if collided(live, junk) == False:
            robots.append(live)
    if robots == []:
        msg =Text("Next Level!", (200, 240), size=36)
        sleep(3)
        remove_from_screen(msg)
        numbots = numbots * 2
        lvl = lvl + 1


def safely_place_player():
    global robots, player
    place_player()
    while collided(player, robots) == True:
        place_player()

def collided(player, robots):
    for robot in robots:
        if player.x == robot.x and player.y == robot.y:
            return True
    return False

def robot_crashed(the_bot):
    for a_bot in robots:
        if a_bot == the_bot:    # we have reached our self in the list
            return False
        if a_bot.x == the_bot.x and a_bot.y == the_bot.y:  # a crash
            return a_bot
    return False

begin_graphics()
numbots = 2
place_robots()
safely_place_player()
finished = False
lvl = 0
highlvl = 1
while finished == False:
    move_player()
    move_robots()
    check_collisions()
    if lvl == highlvl:
        clear_screen()
        place_robots()
        safely_place_player()
        highlvl = highlvl + 1
end_graphics()

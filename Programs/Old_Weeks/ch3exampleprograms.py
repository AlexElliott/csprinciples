#This is where I will be placing all of the example programs because I have already done all the notes in ch3names4numbers.md

#How Expressions are Evaluated
result = 4 + -2 * 3
print(result)

result2 = (4 + -2) * 3
print(result2)

#Driving from Chicago to Dallas
distance = 924.7
mpg = 35.5
gallons = distance / mpg
cost_per_gallon = 3.65
cost_trip = gallons * cost_per_gallon
print("Cost to get from Chicago to Dallas")
print(cost_trip)

#Following the Ketchup Ooze
dripMPH = .028
FPM = 5280.0
dripFPH = dripMPH * FPM
MPH = 60
dripFPM = dripFPH / MPH
Speed4m = 4 / dripFPM
print(f"Ketchup speed in feet per minute: {dripFPM}")
print(f"Ketchup speed to move 4 feet in minutes: {Speed4m}")

#Walking through Assignment more Generally
days_in_week = 7
print(days_in_week)
num_days = 7 * 3
print(num_days)
num_days2 = days_in_week * 3
print(num_days2)

cpa = 2.00
cpe = 9.89
cpd = 7.99
ac = cpa * 10
ec = cpe * 10
total = ac + ec + cpd
print(total)

#Figuring out an Invoice
quantity1 = 2
unit_price1 = 7.56
total1 = quantity1 * unit_price1
quantity2 = 4
unit_price2 = 4.71
total2 = quantity2 * unit_price2
invoice_total = total1 + total2
print(invoice_total)

apples = 4
pears = 3
total_cost =  (0.4 * apples) + (0.65 * pears)
print(total_cost)

cost_per_clip = .05
budget = 4.00
num_paperclips = budget / cost_per_clip
print(num_paper_clips)


product = 1  # Start with multiplication identity
x = 1
while x < 11:
    product = product * x
    x = x + 1
print(product)


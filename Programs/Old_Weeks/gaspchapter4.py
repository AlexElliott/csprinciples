from gasp import *

begin_graphics()
x = 5
y = 5
c = Circle((x, y), 5)


update_when('key_pressed')

while True:
    key = update_when('key_pressed')
    if key == 'q':
        y = y + 4
        x = x - 4
        move_to(c, (x, y))
    elif key == 'e':
        y = y + 4
        x = x + 4
        move_to(c, (x,y))
    elif key == 'z':
        y = y - 4
        x = x - 4
        move_to(c, (x,y))
    elif key == 'c':
        y = y - 4
        x = x + 4
        move_to(c, (x, y))
    elif key == 'w':
        y = y + 4
        move_to(c, (x, y))
    elif key == 'a':
        x = x - 4
        move_to(c, (x, y))
    elif key == 'd':
        x = x + 4
        move_to(c, (x, y))
    elif key == 's':
        y = y - 4
        move_to(c, (x, y))
    elif key == 'v':
        break          # See Sheet L if you aren't sure what this means

end_graphics()

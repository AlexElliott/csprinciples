# Step 1: Initialize accumulator
sum = 0   # Start with nothing (addition identity value)

# Step 2: Get data
numbers = range(0,11)

# Step 3: Loop through the data
for number in numbers:
    # Step 4: Accumulate
    sum = sum + number
# Step 5: Display result
print(sum)



def sum_list(num_list):
    sum = 0 
    for number in num_list:             # Loop through each number
        sum = sum + number         # Accumlate the total
    return sum

num = range(1,11)                 # Create a list of numbers
print(sum_list(num))          # Print the call result to test


def who_knows_why(end_num):
    end_num = end_num + 1
    evens = range(0, end_num, 2)
    odds = range(1, end_num, 2)
    sum = 0
    product = 1
    for number in odds:
        sum = sum * number
    for number in evens:
        product = product + number
    difference = sum - product
    average = (sum + product)/2
    averageD = difference / average
    return averageD
print(who_knows_why(10))

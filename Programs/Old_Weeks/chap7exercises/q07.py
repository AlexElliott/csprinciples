def product(numbers):
    product = 1  # Start out with 1
    for number in numbers:
        product = product * number
    return product
numbers = [1, 2, 3, 4, 5]
print(product(numbers))


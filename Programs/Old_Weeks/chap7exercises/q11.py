def sumevens(lastNum):
    sum = 0  # Start out with nothing
    numbers = range(0,lastNum+1,2)
    for number in numbers:
        sum = sum + number
    return(sum)
to_num = 20
print(sumevens(to_num))

def num_fun(start_num, end_num):
    end_num = end_num + 1
    numbers = range(start_num,end_num)
    product = 1
    sum = 0
    for number in numbers:
        product = product * number
        sum = sum + number
        average = (sum + product)/2
    return average
    

print(num_fun(2,4))

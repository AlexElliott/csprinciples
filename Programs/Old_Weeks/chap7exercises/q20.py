def num_fun(start_num, end_num):
    end_num = end_num + 1
    numbers = range(start_num,end_num)
    total = len(numbers)
    sum = 0
    for number in numbers:
        sum = sum + number
        average = sum / total
    return average
    

print(num_fun(2,4))

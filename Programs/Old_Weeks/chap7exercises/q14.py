def sum_evens(from_num, to_num):
    # Step 1: Initialize accumulator
    product = 0 

    # Step 2: Get data
    numbers = range(from_num, to_num, 2)

    # Step 3: Loop through the data
    for x in numbers:
        # Step 4: Accumulate
        product = product + x

    # Step 5: Return result
    return product

# Step 6: Print the result of calling the function
print(sum_evens(6, 10))



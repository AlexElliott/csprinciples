from random import randint 
right = 0

diff = int(input("What difficulty would you like? Type 1 for Hard and 2 for Easy. "))
nq = int(input("How many questions would you like? "))

if diff == 1:
    print("Hard")
    for x in range(nq):
        num1 = randint(43, 1000)
        num2 = randint(43, 1000)

        question = f"what is {num1} times {num2}? "
        response = (int(input(question)))
        answer = num1 * num2

        if response == answer:
            print("You got it!")
            right = right + 1
        else:
            print (f"Oh no. The correct answer was {answer}")
    print(f"I asked you {nq} questions and you got {right} correct")

    print("Well Done")

else:
    print("Easy")
    for x in range(nq):
        num1 = randint(1, 10)
        num2 = randint(1, 10)

        question = f"what is {num1} times {num2}? "
        response = (int(input(question)))
        answer = num1 * num2
        if response == answer:
            print("You got it!")
            right = right + 1
        else:
            print (f"Oh no. The correct answer was {answer}")
    print(f"I asked you {nq} questions and you got {right} correct")

    print("Well Done")

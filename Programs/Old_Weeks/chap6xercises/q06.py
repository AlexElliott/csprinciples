def move(turtle, size, dis):
    turtle.pensize(size)
    turtle.forward(dis)
    turtle.right(90)
    turtle.forward(dis)
    turtle.right(90)
    turtle.forward(dis)
    turtle.right(90)
    turtle.forward(dis)
    turtle.right(90)

from turtle import *
space = Screen()
t = Turtle()
move(t, 100, 90)


def rectangle(turtle, length, width):
    turtle.forward(length)
    turtle.right(90)
    turtle.forward(width)
    turtle.right(90)
    turtle.forward(length)
    turtle.right(90)
    turtle.forward(width)
    turtle.right(90)

from turtle import *    # use the turtle library
space = Screen()        # create a turtle screen
malik = Turtle()        # create a turtle named malik
rectangle(malik, 100, 50)           # draw a rectangle with malik


def madlib(noun, verb, bp):
    print(f"What came first, the chicken or the {noun}?")

    print(f"Basketball is the best {bp} in the world")
    print(f"I like my donuts with extra {verb} on them.")

print("Time for some madlibs")
noun = (input("Give me a noun: "))
bp = (input("Give me a Body Part: "))
verb = (input("Give me a verb: "))
madlib(noun, verb, bp)

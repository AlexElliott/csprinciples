from turtle import *

#Make screen and name turtle
space = Screen()          
alex = Turtle()            

#Square
alex.forward(100)     
alex.right(90)             
alex.forward(100)          
alex.right(90)            
alex.forward(100)          
alex.right(90)              
alex.forward(100)          

#Roof
alex.right(90)
alex.forward(100)    
alex.right(-120)         
alex.forward(100)       
alex.right(-120)         
alex.forward(100)    
alex.right(-120)

#Chimney
alex.right(-60)
alex.forward(75)
alex.left(30)
alex.forward(25)
alex.left(90)
alex.forward(25)
alex.left(90)
alex.forward(66)

#Door
alex.right(25)
alex.forward(28)
alex.right(65)
alex.left(90)
alex.forward(100)
alex.left(90)
alex.forward(25)
alex.left(90)
alex.forward(75)
alex.right(90)
alex.forward(50)
alex.right(90)
alex.forward(75)

#Spinning Dance
alex.right(1080)

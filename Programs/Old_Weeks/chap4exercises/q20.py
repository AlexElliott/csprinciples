#Write code to print out the statement “Hi my name is Bob and I am 2” using only string methods or string slicing. You must get every part of the new string from the given strings.

s1 = "hi"
s2 = "My namesake is Bob, and he and I love to eat ham."

hi = s1.capitalize()
My = s2.lower()
my = My[0:2]
name = s2[3:7]
isBob = s2[12:18]
andI = s2[27:32]
am = s2[46:48]
two = len(s1)

print(f"{hi} {my} {name} {isBob} {andI} {am} {two}") 


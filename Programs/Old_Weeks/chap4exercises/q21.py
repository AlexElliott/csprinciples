print("Time for some madlibs")

noun = (input("Give me a noun: "))

print(f"What came first, the chicken or the {noun}?")

bp = (input("Give me a Body Part: "))

print(f"Basketball is the best {bp} in the world")

verb = (input("Give me a verb: "))

print(f"I like my donuts with extra {verb} on them.")

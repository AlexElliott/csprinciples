import matplotlib.pyplot as plt

# Program is working well

# This program is a compound interest calculator that is graphed.

# Here there are inputs that asks the user the values it needs to calculate the interest.

years = int(input("How many years do you plan on investing? "))
interest = float(
  input("What do you expect the average yearly interest rate would be? "))
moneydepo = int(
  input("How much money will you be depositing into the investment? "))
timeperiods = int(input("How many times per year does the interest happen? "))

# Here the zeroyears variable is assigned to the value of 1 as it will be added to the set of the x values used in the graph, and it needs to count up from 1, not 0.

zeroyears = 0
loopyears = 0

# The interest here is divided by 100 to make the math accurate as you can then input the integer percentage value when prompted.

interest = interest / 100

# These are our empty lists for our x and y values for the graph that will be added on to with the loop. The variable moneylist is our total account balance and countyears is each year that goes by.

moneylist = []
countyears = []

# This loop will continue until zeroyears reaches the amount of years specified by the user multiplied by the amount of interest period they also specify (It is essentially our exponent as it multiplies itself as many times as the loop runs). The loop will also append each value of moneydepo (our account balance) into the moneylist list, as well as its corresponding zeroyear into the countyears

while zeroyears < (years):
  moneydepo = moneydepo * (1 + (interest / timeperiods))
  loopyears += 1
  print(zeroyears)
  if loopyears % timeperiods <= 0:
    zeroyears += 1
    moneylist.append(moneydepo)
    countyears.append(zeroyears)
    

# These functions will create the plot with the two lists as the x axis and y axis. There are also functions that set the x axis label and the y axis label, as well as the graph title. Then, plt.show() will display the graph in the output.
"""
plt.figure(figsize=(9, 3))

plt.subplot(111)
plt.bar(countyears, moneylist)
plt.subplot(112)
plt.plot(countyears, moneylist)
plt.xlabel("Years")
plt.ylabel("USD")
plt.suptitle("Estimated Yearly Balance")
plt.xlim(.5, zeroyears / 10 + .5)
plt.show()

"""



plt.figure(figsize=(14, 3))
plt.subplot(131)
plt.bar(countyears, moneylist)
plt.xlabel("Years")
plt.ylabel("USD")
plt.xlim(.5, zeroyears / 10 + .5)
plt.subplot(132)
plt.scatter(countyears, moneylist)
plt.subplot(133)
plt.plot(countyears, moneylist)
plt.suptitle("Estimated Yearly Balance")
plt.show()

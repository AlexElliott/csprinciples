# Quiz 1

## Question 5: A programmer has developed an algorithm that computes the sum of integers taken from a list. However, the programmer wants to modify the code to sum up even numbers from the list. What programming structure can the programmer add to do this?

- [ ] a. Sequencing
- [ ] b. Iteration
- [ ] c. Searching
- [ ] d. Selection

### My answer:
I answered Iteration, as iteration is when you plan -> design -> implement -> Test -> Evaluate -> then back to planning. I had thought that this was the best option as the programmer was continuosly developing the code.

### Correct answer:
The answer is Selection, as the programmer is selecting values to add together.

## Question 7: Every night at 12:00 am, you notive that some files are being uploaded from your PC device to an unfamiliar website without your knowledge. You check the file and find that these files are from the day's work at the office. What is this an example of?

- [ ] a. Phishing attack
- [ ] b. Keylogger attack
- [ ] c. Computer Virus
- [ ] d. Malware insertion point

### My answer:
I answered Keylogger Virus as I had thought that the website was taking the files and that the hacker had used a keylogger to take his account credentials. 

### Correct answer:
The answer is Computer Virus as it runs on the PC and can upload files. 

# Quiz 2

## Question 4: What is displayed using the following DISPLAY(expressions) abstrations?
```
list1 = [11,35,6]
DISPLAY(List1[2])
```
- [ ] a. An error message is produced, and the program will terminate.
- [ ] b. 35
- [ ] c. 11
- [ ] d. 6

### My answer:
I answered 6 as I had thought that it would start at 0 and then cound on.

### Correct answer:
The correct answer is 35, as it is the second number in the list.

## Question 9: Using binary search, how many iterations would it take to find the letter w?
```
str = [a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z]
```
- [ ] a. 2
- [ ] b. 3
- [ ] c. 23
- [ ] d. 24

### My answer:
I had answered 24 as I miscounted what number w is in the alphabet and I was counting each letter as 1 itteration. 

### Correct answer:
The correct answer is 3, as binary search splits the list in half and searches the first half for the object. If it is not in the first half it then splits the second half and repeats. 

# Quiz 3

## Question 6: The same task is performed through sequntial and parallel computing models. The time taken by the sequential setup was 30 seconds, while it took the parallel model only 10 seconds. What will be the speedup parameter for working with the parallel model from now on?

- [ ] a. 0.333
- [ ] b. 3
- [ ] c. 300
- [ ] d. 20

### My answer:
I answered 3 as I thought to get the speedup parameter you divide the sequential setup time by the parallel models time.

### Correct answer:
The correct answer is 0.333 as to get the speedup parameter it is multiplication, so it would be 30 * 0.333 = 10.

# Quiz 4

All my answers were correct.


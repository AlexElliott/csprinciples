from svg_turtle import SvgTurtle


colors=["red","yellow","black"]

gus = SvgTurtle(500, 500)
gus.speed(200)
for x in range(2000):
    gus.pencolor(colors[x%3])
    gus.width(2)
    gus.forward(x*2)
    gus.left(121)

gus.save_as('gus.svg')

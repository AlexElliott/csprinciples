def factorial(n):
    """
      >>> factorial(3)
      6
      >>> factorial(7)
      5040
      >>> factorial(24)
      620448401733239439360000
    """



    fact = 1                    
    for i in range(n, 1, -1):   
        fact = fact * i          
    return fact





if __name__ == '__main__':
    import doctest
    doctest.testmod()

# Tips to Ace the APCSP Exam

## Good Information to Know

### Multiple Choice section
  - The Multiple Choice section is worth 70% of the grade
  - There are 70 questions in the multiple choice test
  - The test is on the 5 Big Ideas we learned throughout the year
  - 57 single-select multiple-choice
  - 5 single-select with reading passage about a computing innovation
  - 8 multiple-select multiple-choice: select 2 answers


### This table shows the score, if the score recived qualifies for credits, and the percentage of the people who recived that score.

| Score | Meaning             | Percentage of people who get this score |
| ----- | ------------------- | --------------------------------------- |
| 5     | Extremely qualified | 10.9%                                   |
| 4     | Well qualified      | 23.6%                                   |
| 3     | Qualified           | 37.1%                                   |
| 2     | Possibly qualified  | 19.8%                                   |
| 1     | No Recommendation   | 8.6%                                    |

# We went in depth into the [Test Guide Website](https://www.test-guide.com/ap-computer-science-principles-practice-exam.html)
This website has nine sections total. Each section is very useful.

## First Section
This section talks about how not many students have access to computer science classes, and how very few people actually want to go into the computer science field.

## Practice test
- The tests are 10 questions long
- After the test it gives you a brief overview of the question and its answer and why that is its answer
- The test gives you a score, which makes it easier to track progress

## Third Section
This section includes some extra resources to study with.
| Resource                                   | Question Amount                  |
| ------------------------------------------ | -------------------------------- |
| 2014 College Board AP CSP Sample Questions | 20 multiple-choice, 2 two-answer |
| APa CSP Practice Exam 1 Full               | 66 multiple-choice, 8 two-answer |
| College Board AP CSP Past Exam Questions   | Numerous questions               |

## Fourth Section
This section gives some background information on the APCSAP test. Such as the test this year uses pseudocode and not java anymore, and how it focuses on the fundemental coding aspects.

<!-- The APCSP focuses on the principles of computer science. Passing the class (3 or higher), will likely not have to take another CS intro class in collage. --->

## Fifth Section
This goes into detail about the specific portions of the exam and what they are worth. It also goes into depth about the questions themselves, how the answers will be. For example multiple choice, selecting two true answers, answer about a code or a passage. Basically restates what we said at the begining of this presentation.

## Sixth Section
- This section teaches you how to use a practice exam to best support yourself and prepare for the test.
- The best practice tests are detailed and difficult

## Seventh Section
- This sections talks about studying methods
- The first method you can use is to create or find flashcards to use for review.
- Take AP computer science principles practice exams
- Reviewing a Study guide

## The Rest of the sections
- They are primaraly about common questions involving the APCSP exam, such as what to study on and when the exam is.
- Is the exam is hard
  - However, the AP CSP has one of the lowest perfect score rates at a low 12.4%. If you want a perfect score, it is important to study hard and perform well on the computer program portion.

## Our Favorite Questions

### 1. Chicago recently hired a firm to install security cameras with facial recognition software around Central Park. However, a brawling incident at the park led to the arrest of an innocent person based on the recognized facial features of one of the belligerents.
What should the firm do to remove the bias?

- [ ] A. Improve the training materials used for training and interpreting by the software
- [ ] B. Use photos from a more diverse community for software training
- [ ] C. Ask lawmakers to create stronger regulations for how facial recognition software is used
- [ ] D. Add new team members from diverse communities to help stop biased operations of the software

### 2. What will be the output at the three highlighted points in the following figure of a Boolean logic circuit algorithm if A= 1, B=0, C= 0, and D=1?
![circuit.png](circuit.png)

- [ ] A. 1, 1, 0
- [ ] B. 1, 1, 1
- [ ] C. 1, 0, 0
- [ ] D. 0, 1, 0

### 3. An author wants to publish a new e-book under a protective license. Which of the following scenarios (I, II, and III), if they exist, would benefit the most from using a Creative Commons License?

I. The author intends to allow others to modify his e-book  
II. The author intends to stop others from sharing his e-book in peer-to-peer networks     
III. The author intends to make his e-book available for free download.

- [ ] A. I
- [ ] B. II
- [ ] C. II and III
- [ ] D. I and III

## Answers!!
1. B
   - The software has to be trained through pictures/photos of people from all communities and diverse populations. The wider scope of photos will help the software learn not to discriminate between different sets of people and avoid bias.
2. A
   - The XOR gate gives a high output because both terminals have different states. The NAND gate gets different states at its input (a high and low) and will give out a high output.
The NOR gate gets two high values and thus gives a low value.
3. D
   - Situations I and III would benefit greatly from using a Creative Commons license instead of a traditional copyright license. Both of these situations can be allowed and controlled under the CCL.

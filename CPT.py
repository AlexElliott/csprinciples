import os
import time
from random import randint
states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming']
capitals = ['Montgomery', 'Juneau', 'Phoenix', 'Little Rock', 'Sacramento', 'Denver', 'Hartford', 'Dover', 'Tallahassee', 'Atlanta', 'Honolulu', 'Boise', 'Springfield', 'Indianapolis', 'Des Moines', 'Topeka', 'Frankfort', 'Baton Rouge', 'Augusta', 'Annapolis', 'Boston', 'Lansing', 'Saint Paul', 'Jackson', 'Jefferson City', 'Helena', 'Lincoln', 'Carson City', 'Concord', 'Trenton', 'Santa Fe', 'Albany', 'Raleigh', 'Bismark', 'Columbus', 'Oklahoma City', 'Salem', 'Harrisburg', 'Providence', 'Columbia', 'Pierre', 'Nashville', 'Austin', 'Salt Lake City', 'Montpelier', 'Richmond', 'Olympia', 'Charleston', 'Madison', 'Cheyenne']
dupelist = ['Montgomery', 'Juneau', 'Phoenix', 'Little Rock', 'Sacramento', 'Denver', 'Hartford', 'Dover', 'Tallahassee', 'Atlanta', 'Honolulu', 'Boise', 'Springfield', 'Indianapolis', 'Des Moines', 'Topeka', 'Frankfort', 'Baton Rouge', 'Augusta', 'Annapolis', 'Boston', 'Lansing', 'Saint Paul', 'Jackson', 'Jefferson City', 'Helena', 'Lincoln', 'Carson City', 'Concord', 'Trenton', 'Santa Fe', 'Albany', 'Raleigh', 'Bismark', 'Columbus', 'Oklahoma City', 'Salem', 'Harrisburg', 'Providence', 'Columbia', 'Pierre', 'Nashville', 'Austin', 'Salt Lake City', 'Montpelier', 'Richmond', 'Olympia', 'Charleston', 'Madison', 'Cheyenne']

wrongstates = []
wrongcapitals = []
onetimestates = []
storeddupelist = dupelist
onetimecapitals = []
round = False

def Begin(play, playedalready):
  global dupelist
  global amount
  if playedalready == False:
    amount = int(input("How many questions do you want to be asked? \n"))   
    if amount < 1 or amount > 50:  
      print("Enter a number between 1-50 next time. \n")
      Begin(play, playedalready)
  elif playedalready == True:
    if play == "yes":
      amount = int(input("How many questions do you want to be asked in round 2? \n"))
      print("Remember this is your second round!! Try to beat your score from the first round")
      if amount < 1 or amount > 50:  
       print("Enter a number between 1-50 next time. \n")
       Begin(play, playedalready)
    else:
      exit()
    
def Multichoicefunc():
  global correctstate
  global answerlist
  global amount
  global randpair
  global correctstate
  global correctcapital
  # This sets a random integer for the pair of the correct state and its capital, as they have the same position in their respective lists.
  randpair = randint(0, len(dupelist))
  correctstate = states[randpair]
  correctcapital = capitals[randpair]
  # This insures that the correct capital is not chosen as a fake one, by removing it from the duplicate list of fake answers.
  del dupelist[randpair]
  # This assigns fake answers to variables and removes that fake answer from the list of fake answers, that way there will be no duplicate answers.
  wrongrandint = randint(0, len(dupelist))
  wrongcap1 = dupelist[wrongrandint]
  dupelist.remove(dupelist[wrongrandint])
  wrongrandint = randint(0, len(dupelist))
  wrongcap2 = dupelist[wrongrandint]
  dupelist.remove(dupelist[wrongrandint])
  wrongrandint = randint(0, len(dupelist))
  wrongcap3 = dupelist[wrongrandint]
  dupelist.remove(dupelist[wrongrandint])
  answerlist = []
  answerlist.append(correctcapital)
  answerlist.append(wrongcap1)
  answerlist.append(wrongcap2)
  answerlist.append(wrongcap3)


counter = 0

def Questions():
  global counter
  os.system('clear')
  print("What is the capital of this state:")
  print(correctstate + "\n")
  
  for i in range(0,4):
    randomanswer = randint(0, len(answerlist))
    print(answerlist[randomanswer-1])
    del answerlist[randomanswer-1]
  UserAnswer = input("\nWhat is the correct answer? (Please type the name of the Capital):  \n")
  if UserAnswer == correctcapital:
    print("That is correct. ")
    counter += 1
    time.sleep(3)
    del states[randpair]
    del capitals[randpair]
  else:
    print("That is incorrect. ")
    time.sleep(3)
    
  

def Again():

  ready = (input("\n Do you want to play another round? \n"))
  if str(ready == 'yes' or str(ready) == 'Yes' or str(ready) == 'y' or str(ready) == 'Y'):
    print("Clearing Console.......")
    time.sleep(5)
    os.system('clear')
    Begin()
  else:
    print("Invalid input")
    Again()
  if str(ready == 'no' or str(ready) == 'No' or str(ready) == 'n' or str(ready) == 'N'):
    print("Ok, thanks for playing! ")
    print("Clearing Console")
    time.sleep(5)
    os.system('clear')
  else:
    print("Invalid input")
    Again()

def Quiz():
  global amount
  global round
  plays = input("Do you want to play\n")
  Begin(plays, round)
  while amount > 0:
    Multichoicefunc()
    Questions()
    amount -= 1
  print("You got " + str(counter) + " question(s)  correct")
  time.sleep(3)
  round = True
    

def Quiz2():
  global amount
  global round
  played = input("Do you want to play again?\n")
  Begin(played, round)
  while amount > 0:
    Multichoicefunc()
    Questions()
    amount -= 1
  print("You got " + str(counter) + " question(s)  correct")
  time.sleep(3)

Quiz()
os.system('clear')
Quiz2()
print("Thanks for playing!!")
